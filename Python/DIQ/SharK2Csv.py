import argparse
import os
import sys
import csv
from scapy.fields import SignedShortField
from scapy.layers.inet import *
from scapy.packet import *
from scapy.utils import RawPcapReader

from diq_packet import IqDataList, IQ


def process_pcap(file_name):
    client = '192.168.1.1:49152'
    server = '192.168.1.2:49153'
    i =[]
    q= []
    trgCount = []
    (client_ip, client_port) = client.split(':')
    (server_ip, server_port) = server.split(':')

    print('Opening {}...'.format(file_name))
    bind_layers(UDP, IQ, sport=49152, dport=49153)
    bind_layers(IQ, IqDataList )
    count = 0
    with open('iq.csv', 'w', newline='') as csvfile:
        fieldnames =['Ant_1_i', 'Ant_1_q', 'Ant_2_i', 'Ant_2_q', 'Ant_3_i', 'Ant_3_q', 'Ant_4_i', 'Ant_4_q']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for (pkt_data, pkt_metadata,) in RawPcapReader(file_name):
            count += 1
            ether_pkt = Ether(pkt_data)
            ip_pkt=ether_pkt[IP]
            if (ip_pkt.src != client_ip) and (ip_pkt.dst != server_ip):
                print("Dropped") # Uninteresting source IP address
                continue
            udp_pkt = ip_pkt[UDP]
            iq_pkt = udp_pkt[IQ]
            iqDataPktList = iq_pkt[IqDataList]
            for iq in iqDataPktList.fields["IqDataItem"]:
                writer.writerow(iq.fields)

    print('{} contains {} packets'.format(file_name, count))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PCAP reader')
    parser.add_argument('--pcap', metavar='<pcap file name>',
                        help='pcap file to parse', required=True)
    args = parser.parse_args()

    file_name = args.pcap
    if not os.path.isfile(file_name):
        print('"{}" does not exist'.format(file_name), file=sys.stderr)
        sys.exit(-1)

    process_pcap(file_name)
    sys.exit(0)