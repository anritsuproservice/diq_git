from scapy.all import *
from scapy.layers.inet import *
from scapy.packet import *

from diq_packet import IQ, IqDataList, IqData

interfaces = IFACES.show() # let’s see what interfaces are available. Windows only
print(interfaces)

MT8000_MAC = 'e1:e2:e3:e4:e5:e6'
MT8000_IP = '192.168.1.1'
MT8000_UDP_PORT = 49152

PC_MAC = 'e8:ea:6a:1e:92:28'
PC_IP = "192.168.1.2"
PC_UDP_PORT = 49153

iface = "Intel(R) 82599 10 Gigabit Network Connection"
s = conf.L2socket(iface=iface )
payload = IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / \
             IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData() / IqData()
#packet = Ether(src=PC_MAC, dst=MT8000_MAC) / \
#          IP(dst=MT8000_IP, src=PC_IP, ttl=128) / \
#          UDP(dport=MT8000_UDP_PORT, sport=PC_UDP_PORT) / \
#          IQ() / \
#          IqDataList()/payload
count=0
packet = Ether(src=PC_MAC, dst=MT8000_MAC) / \
         IP(dst=MT8000_IP, src=PC_IP, ttl=128) / \
         UDP(dport=MT8000_UDP_PORT, sport=PC_UDP_PORT) / \
         IQ(trg_count=count) / \
         IqDataList() / payload
raw = packet.build()
print()
while 1:
    sendp(raw, socket=s, verbose=0,realtime=0,count=100000)
    #s.send(packet)