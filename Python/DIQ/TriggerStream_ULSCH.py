from scapy.all import *
from scapy.layers.inet import *
from scapy.packet import *

import scipy.io as scpiio
import scipy.signal as sig
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')

from diq_packet import IQ, IqDataList, IqData


def build_null_packet():
    nullpayload = []
    for x in range(0, 64):
        nullpayload.append(IqData())

    Nullpacket = Ether(src=PC_MAC, dst=MT8000_MAC) / \
                 IP(dst=MT8000_IP, src=PC_IP, ttl=128) / \
                 UDP(dport=MT8000_UDP_PORT, sport=PC_UDP_PORT) / \
                 IQ(trg_count=0) / \
                 IqDataList(IqDataItem=nullpayload)
    logging.info("built null packet")
    return Nullpacket.build()


interfaces = IFACES.show()  # let’s see what interfaces are available. Windows only
print(interfaces)

MT8000_MAC = 'e1:e2:e3:e4:e5:e6'
MT8000_IP = '192.168.1.1'
MT8000_UDP_PORT = 49152

PC_MAC = 'e8:ea:6a:1e:92:28'
PC_IP = "192.168.1.2"
PC_UDP_PORT = 49153
binFile = "C:\\Work\\DIQ\\flls_iq_samples_nr_tc_pucch_1_padded.mat"
# binFile = "C:\\Work\\DIQ\\prach_1slot.mat"
iface = "Intel(R) 82599 10 Gigabit Network Connection"
s = conf.L2socket(iface=iface)


def write_packets_to_file(packetfile, packetlist):
    count = 0
    with open(packetfile, mode="wb+")as outfile:
        for packet in packetlist:
            outfile.write(packet)
            count += 1
        logging.info("written %d packets to %s", count, packetfile)


def read_packets_from_file(packetfile):
    filepacketlist = []
    count = 0
    with open(packetfile, mode="rb")as infile:
        while True:
            bytes = infile.read(1088)
            if not bytes:
                break
            filepacketlist.append(bytes)
            count += 1
    logging.info("read %d packets", count)
    return filepacketlist


def build_ULSCH_packet_list(resample=False, powerscaling=6000):
    packetList = []
    matFile = scpiio.loadmat(binFile)
    signal = matFile.get("sig").flatten()
    upsamplefactor = int(245760 / len(signal))
    if resample:
        signal = sig.resample_poly(signal, upsamplefactor, 1)
        upsamplefactor=1
    count = 0
    signal_samples_per_packet = int(64 / upsamplefactor)
    for x in range(0, int(len(signal) / signal_samples_per_packet)):
        iqPayload = []
        for y in range(0, signal_samples_per_packet):
            iq = signal[count]
            for z in range(0, upsamplefactor):
                iqPayload.append(IqData(Ant_1_i=int(iq.real * powerscaling), Ant_1_q=int(iq.imag * powerscaling)))
            count += 1

        packet = Ether(src=PC_MAC, dst=MT8000_MAC) / \
                 IP(dst=MT8000_IP, src=PC_IP, ttl=128) / \
                 UDP(dport=MT8000_UDP_PORT, sport=PC_UDP_PORT) / \
                 IQ() / \
                 IqDataList(IqDataItem=iqPayload)
        packetList.append(packet.build())
    logging.info("Built {} packets".format(len(packetList)))

    return packetList


rawNull = build_null_packet()
logging.info("building packet list")
# packetfile = "C:\\Work\\DIQ\\PRACH_1slot5Mhz.ether"

# packetfile = "C:\\Work\\DIQ\\20MHzULSCH.ether"
packetfile = "C:\\Work\\DIQ\\20MHzPUCCH_F2_111_ZOH.ether"
#packetfile = "C:\\Work\\DIQ\\20MHzPUCCHS.ether"

#packetlist = build_ULSCH_packet_list(resample=True, powerscaling=6000)
#write_packets_to_file(packetfile, packetlist)
packetlist = read_packets_from_file(packetfile)
logging.info("IQ loaded")
timing_advance = 49

ul_start_sfn = 8

logging.info("sending null packets")
sendp(rawNull, socket=s, verbose=0, realtime=0, count=(38400 * ul_start_sfn + 3840 - timing_advance))
# sendp(rawNull, socket=s, verbose=0, realtime=0, count=10)
logging.info("SendingIQ")
count = 0
while count <= 5:
    for rawpacket in packetlist:
        sendp(rawpacket, socket=s, verbose=0, realtime=0)
    sendp(rawNull, socket=s, verbose=0, realtime=0, count=(3840 * 9))
    count += 1
