from scapy.all import *
from scapy.fields import SignedShortField
from scapy.layers.inet import *
from scapy.packet import *

class IQ(Packet):
    name = "IQ"
    fields_desc = [IntField("trg_count", None),
                   ShortField("frame", None),
                   IntField("reserved_1", None),
                   IntField("reserved_2", None),
                   IntField("reserved_3", None),
                   ShortField("dbg_for_syn", None),
                   ShortField("dbg_for_an", None),
                   ]


class IqData(Packet):
    name = "IqData"
    fields_desc = [SignedShortField("Ant_1_i", None),
                   SignedShortField("Ant_1_q", None),
                   SignedShortField("Ant_2_i", None),
                   SignedShortField("Ant_2_q", None),
                   SignedShortField("Ant_3_i", None),
                   SignedShortField("Ant_3_q", None),
                   SignedShortField("Ant_4_i", None),
                   SignedShortField("Ant_4_q", None),
                   ]

    def extract_padding(self, p):
        return b"", p

class IqDataList(Packet):
     name = "IqDataList"
     fields_desc =  [PacketListField("IqDataItem", [], IqData, ), ]
